﻿using Microsoft.AspNetCore.Mvc;
using PTFibo.Common.Messages;
using PTFibo.Common.Services;
using PTFibo.WebApi.Infrastructure;

namespace PTFibo.WebApi.Controllers
{
    [Route("[controller]")]
    public class FiboController : Controller
    {
        private readonly IMessageBusFactory _messageBusFactory;
        private readonly IFiboCalculationService _fiboCalculationService;

        public FiboController(IMessageBusFactory messageBusFactory, IFiboCalculationService fiboCalculationService)
        {
            _messageBusFactory = messageBusFactory;
            _fiboCalculationService = fiboCalculationService;
        }
        
        [HttpPost]
        public IActionResult CalculateNextFibo([FromBody] CalculateFiboMessage msg)
        {
            var bus = _messageBusFactory.GetMessageBus();

            var nextValue = _fiboCalculationService.CalculateNext(msg.CurrentValue);
            
            bus.Publish(new CalculateFiboMessage(msg.SessionId, nextValue));
            
            return Ok();
        }
        
        // GET
        public IActionResult Index()
        {
            return new ObjectResult("Hello World");
        }
    }
}