﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using PTFibo.Common.Services;
using PTFibo.WebApi.Infrastructure;

namespace PTFibo.WebApi
{
    public sealed class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            var settings = new Settings("host=localhost");
            
            services.AddSingleton(settings);
            services.AddSingleton<IMessageBusFactory, MessageBusFactory>();
            services.AddSingleton<IMessageBusFactory, MessageBusFactory>();
            services.AddTransient<ICalculationCache, InMemoryCalculationCache>();
            services.AddTransient<IFiboCalculationService, FiboCalculationService>();
            
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseMvc();
        }
    }
}