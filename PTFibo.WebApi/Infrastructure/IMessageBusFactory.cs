﻿using EasyNetQ;

namespace PTFibo.WebApi.Infrastructure
{
    public interface IMessageBusFactory
    {
        IBus GetMessageBus();
    }
}