﻿using System;
using EasyNetQ;

namespace PTFibo.WebApi.Infrastructure
{
    public sealed class MessageBusFactory : IMessageBusFactory
    {
        private Lazy<IBus> _bus;

        public MessageBusFactory(Settings settings)
        {
            _bus = new Lazy<IBus>(() => RabbitHutch.CreateBus(settings.RabbitConnectionString));
        }


        public IBus GetMessageBus()
        {
            return _bus.Value;
        }
    }
}