﻿namespace PTFibo.WebApi.Infrastructure
{
    public sealed class Settings
    {
        public string RabbitConnectionString { get; }

        public Settings(string rabbitConnectionString)
        {
            RabbitConnectionString = rabbitConnectionString;
        }
    }
}    