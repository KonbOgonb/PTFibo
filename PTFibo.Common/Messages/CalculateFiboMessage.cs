﻿using System;

namespace PTFibo.Common.Messages
{
    public sealed class CalculateFiboMessage
    {
        public long CurrentValue { get; }
        
        public Guid SessionId { get; }

        public CalculateFiboMessage(Guid sessionId, long currentValue)
        {
            SessionId = sessionId;
            CurrentValue = currentValue;
        }
    }
}