﻿using System.Collections.Concurrent;

namespace PTFibo.Common.Services
{
    public class InMemoryCalculationCache : ICalculationCache
    {
        private static readonly ConcurrentDictionary<long, long> _dct = new ConcurrentDictionary<long, long>();

        public bool TryGetPrevious(long current, out long previous)
        {
            return _dct.TryGetValue(current, out previous);
        }

        public void StorePrevious(long current, long previous)
        {
            _dct[current] = previous;
        }
    }
}