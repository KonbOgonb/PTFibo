﻿namespace PTFibo.Common.Services
{
    public interface ICalculationCache
    {
        bool TryGetPrevious(long current, out long previous);
        
        void StorePrevious(long current, long previous);
    }
}