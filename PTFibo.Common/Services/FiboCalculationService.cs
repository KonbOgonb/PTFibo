﻿using System;

namespace PTFibo.Common.Services
{
    public class FiboCalculationService : IFiboCalculationService
    {
        private readonly ICalculationCache _calculationCache;

        public FiboCalculationService(ICalculationCache calculationCache)
        {
            _calculationCache = calculationCache;
        }
        
        public long CalculateNext(long current)
        {
            if (current < 0)
            {
                throw new ArgumentOutOfRangeException("Expected value greater or equal than 0");
            }

            if (current == 0)
            {
                return 1;
            }

            if (current == 1)
            {
                return 2;
            }

            if (!_calculationCache.TryGetPrevious(current, out var previous))
            {
                previous = CalculatePrevious(current);
                _calculationCache.StorePrevious(current, previous);
            }

            var next = current + previous;
            _calculationCache.StorePrevious(next, current);

            return next;
        }

        private long CalculatePrevious(long target)
        {
            long previous = 0;
            long current = 1;

            while (current < target)
            {
                var next = current + previous;
                previous = current;
                current = next;
            }

            if (current != target)
            {
                throw new ArgumentException($"Provided value '{target}' does not belong to fibonacci sequence");
            }

            return previous;
        }
    }
}