﻿namespace PTFibo.Common.Services
{
    public interface IFiboCalculationService
    {
        long CalculateNext(long current);
    }
}