﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using PTFibo.Common.Messages;
using PTFibo.Common.Services;

namespace PTFibo.Application
{
    sealed class FiboCalculator
    {
        private readonly HttpClient _httpClient;
        private readonly Settings _settings;
        private readonly FiboCalculationService _fiboCalculationService;

        private Logger _logger = LogManager.GetCurrentClassLogger();

        internal FiboCalculator(HttpClient httpClient, Settings settings, FiboCalculationService fiboCalculationService)
        {
            _httpClient = httpClient;
            _settings = settings;
            _fiboCalculationService = fiboCalculationService;
        }

        public async Task StartFiboCalculation(Guid sessionId, long currentValue)
        {
            try
            {
                _logger.Info($"Started calculation iteration for session '{sessionId}', with value '{currentValue}' in thread '{Thread.CurrentThread.ManagedThreadId}'");
                var nextValue = _fiboCalculationService.CalculateNext(currentValue);
                await StartRemoteFiboCalculation(sessionId, nextValue);
            }
            catch(Exception e) 
            {
                _logger.Error($"Exception during calculation for session '{sessionId}'. Details: '{e}'");
            }
        }

        public void HandleCalculateFiboMessage(CalculateFiboMessage msg)
        {
            var task = new Task(() => StartFiboCalculation(msg.SessionId, msg.CurrentValue));
            task.Start();
        }
        
        private async Task StartRemoteFiboCalculation(Guid sessionId, long currentValue)
        {
            var newMessage = new CalculateFiboMessage(sessionId, currentValue);
            var response = await _httpClient.PostAsJsonAsync(_settings.ApiBaseUrl.TrimEnd('/') + "/fibo", newMessage);
            response.EnsureSuccessStatusCode();
        }
    }
}