﻿namespace PTFibo.Application
{
    sealed class Settings
    {
        public string RabbitConnectionString { get; }  
        
        public string ApiBaseUrl { get; } 
        
        public int DefaultcalculationThreadsCount { get; }

        public Settings(string rabbitConnectionString, string apiBaseUrl, int defaultcalculationThreadsCount)
        {
            RabbitConnectionString = rabbitConnectionString;
            ApiBaseUrl = apiBaseUrl;
            DefaultcalculationThreadsCount = defaultcalculationThreadsCount;
        }
    }
}