﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EasyNetQ;
using PTFibo.Common.Messages;
using PTFibo.Common.Services;

namespace PTFibo.Application
{
    sealed class Startup
    {
        private static HttpClient _client;
        private static Settings _settings;

        static void Main(string[] args)
        {
            _settings = SettingsProvider.GetSettings();
            _client = new HttpClient();
            _client.BaseAddress = new Uri(_settings.ApiBaseUrl);

            var calculationThreadsCount = GetCalculationThreadsCount(args, _settings.DefaultcalculationThreadsCount);

            using (var bus = RabbitHutch.CreateBus(_settings.RabbitConnectionString))
            {
                bus.Subscribe<CalculateFiboMessage>("FiboCalculation", HandleMessage);
                
                Console.WriteLine($"Starting calculation of {calculationThreadsCount} fibo sequences.");
                StartCalculations(calculationThreadsCount);

                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }

        private static void HandleMessage(CalculateFiboMessage msg)
        {
            var fiboCalculator = CreateFiboCalculator();
            fiboCalculator.HandleCalculateFiboMessage(msg);
        }

        private static FiboCalculator CreateFiboCalculator()
        {
            return new FiboCalculator(
                _client,
                _settings,
                new FiboCalculationService(new InMemoryCalculationCache()));
        }

        private static void StartCalculations(int calculationThreadsCount)
        {
            for (var i = 0; i < calculationThreadsCount; i++)
            {
                var task = new Task(() =>
                {
                    var fiboCalculator = CreateFiboCalculator();
                    fiboCalculator.StartFiboCalculation(Guid.NewGuid(), 0);
                });
                task.Start();
            }
        }

        private static int GetCalculationThreadsCount(string[] args, int defaultCount)
        {
            if (int.TryParse(args.FirstOrDefault(), out var calculationThreadsCount))
            {
                return calculationThreadsCount;
            }

            return defaultCount;
        }
    }
}