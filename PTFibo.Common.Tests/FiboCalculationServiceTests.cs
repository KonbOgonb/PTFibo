using System;
using Moq;
using NUnit.Framework;
using PTFibo.Common.Services;

namespace PTFibo.Common.Tests
{
    [TestFixture]
    public class FiboCalculationServiceTests
    {
        private FiboCalculationService _target;
        private Mock<ICalculationCache> _calculationCacheMock;

        [SetUp]
        public void Init()
        {
            _calculationCacheMock = new Mock<ICalculationCache>();
            _target = new FiboCalculationService(_calculationCacheMock.Object);
        }
        
        [Test]
        [TestCase(2, 3)]
        [TestCase(4181, 6765)]
        public void ShouldCalculateNextWithCacheMiss(long current, long expectedNext)
        {
            long tmp;
            _calculationCacheMock.Setup(x => x.TryGetPrevious(It.IsAny<long>(), out tmp)).Returns(false);

            var calculated = _target.CalculateNext(current);
            
            _calculationCacheMock.Verify(x => x.TryGetPrevious(current, out tmp), Times.Once);
            _calculationCacheMock.Verify(x => x.StorePrevious(calculated, current), Times.Once);
            _calculationCacheMock.Verify(x => x.StorePrevious(current, It.IsAny<long>()), Times.Once);
            
            Assert.AreEqual(expectedNext, calculated);
        }
        
        [Test]
        public void ShouldCalculateNextWithCacheHit()
        {
            var current = 13;
            long prev = 8;
            _calculationCacheMock.Setup(x => x.TryGetPrevious(13, out prev)).Returns(true);

            var calculated = _target.CalculateNext(current);
            
            _calculationCacheMock.Verify(x => x.TryGetPrevious(current, out prev), Times.Once);
            _calculationCacheMock.Verify(x => x.StorePrevious(calculated, current), Times.Once);
            
            Assert.AreEqual(21, calculated);
        }
        
        [Test]
        [TestCase(0, 1)]
        [TestCase(1, 2)]
        public void ShouldCalculateNextForLowBorderCases(long current, long expectedNext)
        {
            var calculated = _target.CalculateNext(current);
            
            Assert.AreEqual(expectedNext, calculated);
        }
        
        [Test]
        public void ShouldThrowWhenNumberIsNotFibonacci()
        {            
            Assert.That(() => _target.CalculateNext(11), 
                Throws.TypeOf<ArgumentException>());
        }
        
        [Test]
        public void ShouldThrowWhenNumberIsNegative()
        {
            Assert.That(() => _target.CalculateNext(-1), 
                Throws.TypeOf<ArgumentOutOfRangeException>());
        }
    }
}