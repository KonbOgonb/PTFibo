PTFibo.WebApi - web api endpoint. Receives post message for calculating next number. Calculates it and send response via message bus.
PTFibo.Application - console application. Starts calculation sessions and process calculation requests from message bus.
PTFibo.Common - shared part, contains message type used in bus and service for calculating next fibo number.

Target framework: netcoreapp2.0.

PTFibo.WebApi should be started before PTFibo.Application. 
The progress of calculations is outputed via NLog to console target. So it will print some information to PTFibo.Application console. By default application starts to calculate 100 fibo sequences, this number can be provided during start up.
Right now max calculated number is limited by long type.